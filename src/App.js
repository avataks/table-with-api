import React, { Component } from "react";
import "./components/table/style.css"

import "./App.css"
import { Table } from "./components/table/table"


export default class App extends Component {
  state = {
    cardId: null,
    tableExist: true,
  };

  onSetCardId = (id) => this.setState({ cardId: id })


  render() {
    return (
      <div>
        <button onClick={() => this.setState({ tableExist: !this.state.tableExist })}>Delete table</button>
        {this.state.tableExist ? <Table /> : null}
      </div>
    )
  }
}