import React, {Component} from 'react'

import "./table.css"

export class Table extends Component{
state = { posts:[], load:true, page:1,itemsPerPage:10,keyWords:"",startTime:0,removedItems:0};

componentDidMount(){
    fetch('https://jsonplaceholder.typicode.com/posts').then((res)=>res.json()).then((json)=>{
        this.setState({posts:json,load:false,startTime:Date.now()})
    })
}

componentWillUnmount(){
const time = Date.now();
const TotalTime = (time - this.state.startTime)/1000;
alert(`Total time on page: ${TotalTime}s, removed items: ${this.state.removedItems} `)
}

renderTable = () => {
    const LastIndex = this.state.page*this.state.itemsPerPage;
    let portion;
    if(!this.state.keyWords){
        portion = [...this.state.posts].slice(LastIndex- this.state.itemsPerPage, LastIndex)
    }else{
        portion = [...this.state.posts].filter((el)=> el.title.toLowerCase().includes(this.state.keyWords.toLowerCase()) ||
        el.body.toLowerCase().includes(this.state.keyWords.toLowerCase()))
    }
    return portion.map(({id,userId,title,body})=>
    <tr key={`${id}-${title}`}>
        <th>{id}</th>
        <th>{userId}</th>
        <th>{title}</th>
        <th>{body}</th>
        <th><button className="remove" onClick={this.onRemoveItem(id)}>D</button></th>
    </tr>)
}

onChangePage = (dir) => () => {
    this.setState({page:this.state.page + dir})
}

onChangePerPage = (e) => {
    this.setState({itemsPerPage: e.target.value, page:1})
}

onSearch = (e) => {
    this.setState({keyWords:e.target.value})
}

onRemoveItem = (id) => () =>{
    const _posts = [...this.state.posts];
    const index = _posts.findIndex((el)=>el.id === id);
    _posts.splice(index,1);
    this.setState({posts:_posts,removedItems:this.state.removedItems +1});
    alert(`Post with id: ${id} has been removed`)
}

render(){
    const {load,page,posts,itemsPerPage, keyWords} = this.state;
    if(load){
        return <div className="container">...Loadding</div>
    }else{
        return<div className="container">
            <div className="pagination">
                <input className='search' value={keyWords} onChange={this.onSearch}/>
            <button disabled={page===1 || keyWords} className="button" onClick={this.onChangePage(-1)}>
                Prev
                </button>
            <span>{page}/{Math.ceil( posts.length/itemsPerPage)}</span>
            <button disabled={(page === posts.length/itemsPerPage)|| keyWords} className="button" onClick={this.onChangePage(1)}>
                Next
            </button>
            <select disabled={keyWords} value={itemsPerPage} className='button' onChange={this.onChangePerPage}>
                <option value={10}>10 item</option>
                <option value={20}>20 item</option>
                <option value={25}>25 item</option>
            </select>
            </div>
            <table className="table" border = '1'>
            <tbody>
            <tr>
                <th>id</th>
                <th>User id</th>
                <th>Title</th>
                <th>Body</th>
                <th>Remove</th>
            </tr>
            {this.renderTable()}</tbody>
            </table>
            
            </div>
    }
}
}